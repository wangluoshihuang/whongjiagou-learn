let net = require('net');
let socket = new net.Socket();
socket.connect(8080, 'localhost', function () {  // 客户端连上服务器之后，就给服务器发送一个hello
    socket.write('hello');
});
socket.setEncoding('utf8');
socket.on('data', function (data) {  // 监听服务器返回的数据
    console.log(data);
});
setTimeout(function () {
    //要求关闭跟服务器的连接
    socket.end();
}, 5000);